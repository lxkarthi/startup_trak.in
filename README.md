# trak.in Funding Data

## Summary
### Chart
![Scheme](funding_2015_2016_chart.PNG)

### Table
![Scheme](funding_2015_2016_comparison.PNG)


## How to extract/update data
- Visit [trak.in Funding Data](http://trak.in/india-startup-funding-investment-2015/) and open Funding Data For each month.
- Use ["Data Scraper"](https://chrome.google.com/webstore/detail/data-scraper/nndknepjnldbdbepjfgmncbggmopgden?utm_source=chrome-app-launcher-info-dialog) extension in Chrome to extract data from webpages and export as csv files for each month numbered from 01.csv to 12.csv in "year" folder.
- Run `python find_total.py "year" ` to get the table.
- Enter the data in excel and create charts and conditional formatting.