import csv
import sys
import glob, os
dname = sys.argv[1]
os.chdir(dname)
for file in glob.glob("*.csv"):
    f = open(file, 'rt')
    total_fund= 0;
    colno=0
    #colno = int(sys.argv[2])
    try:
        reader = csv.reader(f)
        for row in reader:
            if(colno==0):
                for c in range(len(row)):
                    if "Amount" in row[c]:
                        colno=c
            fund = (row[colno].replace(',',''))
            try:
                val = int(fund)
                total_fund += val
            except ValueError:
                #print(colno)
                #print(fund.encode("utf-8"))
                pass
    finally:
        f.close()
    print(file + "=", total_fund)